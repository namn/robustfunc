from setuptools import setup
name = 'robustfunc'
setup(
    name=name,
    version='0.0.1',
    author='Nam T. Nguyen',
    author_email='namn@bluemoon.com.vn',
    url='https://bitbucket.org/namn/robustfunc/overview',
    description='Robustfunc provides a set of decorators to add common features to existing functions.',
    long_description='Robustfunc provides a set of decorators to add common features to existing functions.',
    platforms='Any',
    package_dir={'':'.'},
    packages=[''],
    package_data={'': ['README', 'LICENSE']},
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ]
)
