'''Collection of decorators to add common extra functionalities such as retry,
throttle, coalesc to existing functions.

Distributed under the BSD license.

Copyright 2012 Nam T. Nguyen

'''

import hashlib
import sys
import time
import threading
import unittest
import weakref


class Call(object):

    def __init__(self, orig_func):
        self.orig_func = orig_func

    def _do(self, *args, **kw_args):
        return self.orig_func(*args, **kw_args)

    def __call__(self, *args, **kw_args):
        self.__before__(*args, **kw_args)
        try:
            ret = self._do(*args, **kw_args)
        except:
            exc = sys.exc_info()
            self.__raised__(exc, *args, **kw_args)
            # re-raise with proper traceback
            raise exc[1], None, exc[2]
        else:
            self.__returned__(ret, *args, **kw_args)
            return ret

    def __before__(self, *args, **kw_args):
        pass

    def __raised__(self, exception, *args, **kw_args):
        pass

    def __returned__(self, return_value, *args, **kw_args):
        pass


class RetryCall(Call):

    def __init__(self, orig_func, nr_tries=3):
        super(RetryCall, self).__init__(orig_func)
        self.nr_tries = nr_tries

    def _do(self, *args, **kw_args):
        for i in xrange(self.nr_tries):
            try:
                return self.orig_func(*args, **kw_args)
            except:
                if i == self.nr_tries - 1:
                    raise
class retry(object):
    def __init__(self, nr_tries=3):
        self.nr_tries = nr_tries
    def __call__(self, orig_func):
        return RetryCall(orig_func, self.nr_tries)


class Throttled(Exception): pass


class ThrottlingCall(Call):

    def __init__(self, orig_func, accept_func):
        super(ThrottlingCall, self).__init__(orig_func)
        self.accept_func = accept_func
        self.allowed_time = time.time()

    def _accept_func(self, *args, **kw_args):
        return (self.allowed_time - time.time()) < 2

    def __before__(self, *args, **kw_args):
        self.allowed_time += 2.0 / 20
        if self.allowed_time < time.time():
            self.allowed_time = time.time()
        if not self.accept_func(self, *args, **kw_args):
            raise Throttled()
class throttle(object):
    def __init__(self, accept_func=ThrottlingCall._accept_func):
        self.accept_func = accept_func
    def __call__(self, func):
        return ThrottlingCall(func, self.accept_func)


class CoalesceCall(Call):
    '''Merge multiple calls to the same function (from different threads) into
    one call.

    Consider this definition::

        @CoalesceCall()
        def func(integer):
            return integer

    If we make three separate calls from three theads at about the same time::

        func(1)  # thread 1
        func(1)  # thread 2
        func(1)  # thread 3

    Then ``func`` is actually invoked only once, and the result used in the
    other two calls.

    The wrapped function must be idempotent. It MUST NOT have side effect.

    '''

    class ReturnValue(object):
        slots = ('type_', 'value')
        def __init__(self, type_, value):
            self.type_ = type_
            self.value = value

    def __init__(self, orig_func, serialize_func):
        super(CoalesceCall, self).__init__(orig_func)
        self.serialize_func = serialize_func
        self.lock_map = {}
        self.return_map = weakref.WeakKeyDictionary()
        self.local = threading.local()

    def _serialize(self, *args, **kw_args):
        md = hashlib.new('md5')
        for arg in args:
            md.update('%s' % arg)
        for key in kw_args:
            md.update('%s' % tuple(key, kw_args[key]))
        return md.digest()

    def __before__(self, *args, **kw_args):
        self.local.key = self.serialize_func(self, *args, **kw_args)

    def _do(self, *args, **kw_args):
        ReturnValue = CoalesceCall.ReturnValue
        key = self.local.key
        lock = self.lock_map.setdefault(key, threading.Lock())
        with lock:
            try:
                ret = self.return_map[lock]
            except KeyError:
                try:
                    ret = ReturnValue(0, self.orig_func(*args, **kw_args))
                except:
                    ret = ReturnValue(1, sys.exc_info())
                self.return_map[lock] = ret
                del self.lock_map[key]
        # because we've obtained ret, we do not need to have ref to lock
        # releasing ref to lock early also releases the weakref in return_map
        del lock

        if ret.type_ == 0:
            return ret.value
        else:
            raise ret.value[1], None, ret.value[2]
class coalesce(object):
    def __init__(self, serialize_func=CoalesceCall._serialize):
        self.serialize_func = serialize_func
    def __call__(self, orig_func):
        return CoalesceCall(orig_func, self.serialize_func)


class RetryTest(unittest.TestCase):

    def test_return_value(self):

        def ret_func(i):
            return i

        func = retry(3)(ret_func)
        self.assertEqual(1, func(1))
        self.assertEqual(2, func(2))

    def test_except(self):

        class exc_func(object):
            def __init__(self, times=3):
                self.times = times
            def __call__(self):
                self.times -= 1
                if self.times > 0:
                    raise Exception(i)
                return self.times

        func = retry(3)(exc_func(2))
        self.assertEqual(0, func())
        func = retry(3)(exc_func(3))
        self.assertEqual(0, func())
        func = retry(3)(exc_func(4))
        self.assertRaises(Exception, func)


class ThrottleTest(unittest.TestCase):

    def test_default_accept(self):

        def func():
            pass

        func = throttle()(func)
        # run it 20 times as a burst
        for i in range(20):
            func()
        # this should raise because it is over limit
        self.assertRaises(Throttled, func)
        # cool down a bit
        import time
        time.sleep(0.2)
        # now it should run again
        func()

    def test_accept(self):
 
        def accept(*args, **kw_args):
            return False

        def func():
            pass

        func = throttle(accept)(func)
        self.assertRaises(Throttled, func)


class CoalesceTest(unittest.TestCase):

    def test_return(self):
        import time

        class call(object):
            def __init__(self):
                self.times = 0
            def __call__(self):
                self.times += 1
                time.sleep(0.2)
                return 42

        orig_func = call()
        func = coalesce()(orig_func)

        self.wrong_value = 0
        def execute():
            if func() != 42:
                self.wrong_value += 1

        ts = []
        for i in range(3):
            ts.append(threading.Thread(target=execute))
        for t in ts:
            t.start()
        for t in ts:
            t.join()

        self.assertFalse(self.wrong_value)
        self.assertEqual(1, orig_func.times)
        self.assertFalse(func.return_map)
        self.assertFalse(func.lock_map)

    def test_exc(self):
        import time

        class call(object):
            def __init__(self):
                self.times = 0
            def __call__(self):
                self.times += 1
                time.sleep(0.2)
                raise Exception(42)

        orig_func = call()
        func = coalesce()(orig_func)

        self.wrong_value = 0
        def execute():
            try:
                func()
                self.wrong_value += 1
            except Exception as e:
                if e.args[0] != 42:
                    self.wrong_value += 1

        ts = []
        for i in range(3):
            ts.append(threading.Thread(target=execute))
        for t in ts:
            t.start()
        for t in ts:
            t.join()

        self.assertFalse(self.wrong_value)
        self.assertEqual(1, orig_func.times)
        self.assertFalse(func.return_map)
        self.assertFalse(func.lock_map)


if __name__ == '__main__':
    unittest.main()
